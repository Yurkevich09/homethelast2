public class CashDesk
{
    private String cashDeskName;

    public CashDesk(String cashDeskName)
    {
        this.cashDeskName = cashDeskName;
    }

    public String getCashDeskName()
    {
        return this.cashDeskName;
    }

    public synchronized String getGoods(Buyer buyer)
    {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < buyer.getGoods().length; i++)
            if (i != buyer.getGoods().length - 1)
                result.append(buyer.getGoods()[i] + ", ");
            else
                result.append(buyer.getGoods()[i]);
        return "Покупатель " + buyer.getName() + " купил: " + result + " в " + this.getCashDeskName();
    }
}