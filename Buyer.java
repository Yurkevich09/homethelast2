public class Buyer extends Thread
{
    private CashDesk[] cashDesks;
    String goods[];

    public Buyer(String name, CashDesk[] cashDesks, String[] goods)
    {
        super(name);
        this.cashDesks = cashDesks;
        this.goods = goods;
        this.start();
    }

    public String[] getGoods()
    {
        return this.goods;
    }

    @Override
    public void run()
    {
        boolean flag = true;
        while (flag)
        {
            for (int i = 0; i < this.cashDesks.length; i++)
            {
                System.out.println(this.cashDesks[i].getGoods(this));
                if (this.isAlive())
                {
                    flag = false;
                    break;
                }
            }
        }
    }
}